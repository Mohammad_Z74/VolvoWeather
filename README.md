# VolvoWeather

I would like to disclose that this code challenge was initially developed for Klarna about 4 weeks ago, and I have recently been contacted by Volvo recruiter. Although I have received an offer from Klarna, I have decided to pursue a position at Volvo instead. In order to make the process faster, I asked the recruiter if I could use the same weather application for my code challenge that I had previously developed for Klarna. It turns out that the challenges were identical. I hope you find this README informative and useful in understanding my technical decisions. I am happy to provide more details during our technical interview.

This is a simple app that shows the weather, with a focus on the code rather than the UI. Let's take a quick look at the app, and then we'll dive into some technical decisions. We can go more in-depth during the demo interview.

This is what the app looks like:

<img src= "./screenshots/1.png" width=250 height=550> <img src= "./screenshots/2.png" width=250 height=550> <img src= "./screenshots/3.png" width=250 height=550> <img src= "./screenshots/4.png" width=250 height=550> <img src= "./screenshots/5.png" width=250 height=550>

## Features
- Shows current weather
- Dark/light mode support 
- Offline mode 
- Simple :D 

## Some technical key points in the project
- Modularized using SPM local package
- Custom network module using new concurrency APIs and Combine
- Custom MVVM-C architecture
- UI/Unit tests
- Includes both UIKit and SwiftUI views
- Offline mode using compiler flag

Ok I will try to write this readme in a simple way so please bear with me.

## Modularization

I'd like to modulrize my applications so I can take benefit of parallelised builds to decrease compile time for developers. However, not all kinds of modularization lead to a decrease in build time. If they're implemented incorrectly, they can actually lead to worse build times. Here are the four main modules in the app:

![Screenshot 2023-03-22 at 11.47.41 AM.png](https://www.dropbox.com/s/xs6fmt1e3jcg6w6/Screenshot%202023-03-22%20at%2011.47.41%20AM.png?dl=0&raw=1)

As you can see, each module is responsible for a particular part of the app. However, it's important to consider the dependencies between these modules. For example, both the Search and Home modules have a dependency on the Network module, since they need to make network requests. Additionally, the Home module needs to have some sort of dependency on the Search module, since the Home screen needs to be updated when the user selects a city. Here's how the dependency graph looks like:

![Screenshot 2023-03-22 at 11.40.37 AM.png](https://www.dropbox.com/s/j2pnb2j6zrw1zp3/Screenshot%202023-03-22%20at%2011.40.37%20AM.png?dl=0&raw=1)

Unfortunately, this dependency graph isn't ideal for modularization, since the Home and Search modules are dependent on each other, and both are dependent on the Network module. This means that if a single change is made to the Search or Network modules, the entire app needs to be recompiled. As you can see in the dependency graph, these modules are vertically dependent on each other. The more vertical dependencies we have, the less benefit we get from modularization in terms of compile time. 
To fix this, we can try to make the dependency graph as horizontal as possible. Instead of having direct dependencies on concrete classes, we can have dependencies on abstract protocols. This way, the Home, Search, and Network modules can be completely independent of each other, and they won't need to be recompiled if any of them are changed. Here's what the new dependency graph looks like:

![Screenshot 2023-03-22 at 11.40.31 AM.png](https://www.dropbox.com/s/ihl0gufzsu2orih/Screenshot%202023-03-22%20at%2011.40.31%20AM.png?dl=0&raw=1)

By introducing the two new modules, searchInterface and NetworkInterface, we have created lightweight protocols that do not contain any concrete implementation. This approach has allowed us to make our dependency graph more horizontal, meaning that changes made to any of the three modules will not affect the other modules. For example, if we make a change to the search module, the home module will not have to be recompiled again.

In this way, our modules are able to communicate with each other through protocols at runtime. Ok, I didn't want to explain this much in a readme and we haven't gone through to rest topics so we can discuss more on this in the demo interview.

## Network layer

In the network module, I've brought a lightweight version of the network layer I recently developed. The main goal is to simplify network calls using new structured concurrency APIs and also Combine support for ease of developers. I have removed some features that was not needed for this project, such as download/upload, custom decoding, custom error handling and different URL encoding types and sslpinning handling.

The network module is responsible for making network calls in our app. It's divided into two main parts: request and response. To achieve this, I've created subclasses for each part. The HTTPClient class is where requests are executed, while HTTPDataLoader is the main class for fetching data. HTTPResponse stores the response callback, potential errors received from the server, and is also responsible for decoding data. In addition, this network module also supports custom logging, giving developers the flexibility to log response as desired.. 

as I pointed out in modulrization section, instead of direct dependency to network module I use networkInterface to able to make network calls. This is how our protocol looks like:

```swift
public protocol NetworkingInterface {
    func fetchData<T: Decodable>(with router: NetworkingRouterProtocol, for dataType: T.Type) async throws -> T
    func fetchDataPublisher<T: Decodable>(with router: NetworkingRouterProtocol, for dataType: T.Type) -> AnyPublisher<T, HTTPResponseError>
}
```
Using this protocol, we can inject a class that conforms to this protocol into our ViewModel to execute network calls. To achieve this, I've used facade pattern. The KLNetworkingProvider class is a wrapper on top of the network module in the app target, which confirms to the NetworkingInterface protocol. This allows us to inject an instance of KLNetworkingProvider into our view models at runtime in our coordinators. A dependency engine is responsible for storing this instance. NetworkingRouterProtocol is responsible for storing all the data that our network module needs to make a network request. (See HomeServiceRouter.swift for an example). Ok again I think I'm explaining too much :D. thanks for bearing with me so far. Note: As you can see, the NetworkingInterface protocol contains both Combine and new async APIs for fetching data. Although this wasn't strictly necessary, I wanted to provide developers with more tools and flexibility :)


### MVVM & Coordinator and other
I have used a custom version of the MVVM-C (Model-View-ViewModel-Coordinator) pattern for the overall architecture of the modules. This allows for easy injection of dependencies into view controllers and view models.
The app has a total of 3 coordinators. The AppCoordinator is where the app starts and injects dependencies into other module coordinators (such as our network wrapper). The HomeCoordinator and SearchCoordinator are responsible for navigating and injecting dependencies into their corresponding modules.
By using the MVVM-C pattern, we can separate concerns and easily manage the flow of data and navigation throughout the app.
I'd like to implement my ViewControllers using code rather than storyboards or XIB files. to achive that you can see I have defined my properties lazily to add them in memory when they are needed. Example: 
```swift
//MARK: subviews
    private lazy var currentTemp: UILabel = {
        let label = UILabel()
        self.view.addSubview(label)
        label.textColor = .systemGray2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        return label
    }()
```
Also for this part of Home I used SwiftUI to impelement its view as it was way easier:
![Screenshot 2023-02-26 at 9.20.30 PM.png](https://www.dropbox.com/s/7afdgl5myjo9mkn/Screenshot%202023-02-26%20at%209.20.30%20PM.png?dl=0&raw=1)

In order to do that, I had to add it as childviewcontroller to HomeViewController (see HomeViewController line 66).
Now let's skip other subviews and go throught ViewModel implementation:
as you can see in the HomeViewController initializer we need an instance of ViewModel in its dependencies:
```swift
// MARK: Initializers
    init(homeViewModel: HomeViewModel) {
        self.viewModel = homeViewModel
        super.init(nibName: nil, bundle: nil)
    }
```
And here is ViewModel initializer
```swift
// MARK: Initializers
    init(dataProvider: HomeServiceProtocol) {
        self.dataProvider = dataProvider
        super.init()
        locationManager.delegate = self
    }
```
As you can see viewModel needs a dataProvider to able to show data and needs to confirm to HomeServiceProtocol. By this, any class that is confirming to this protocol can provide data for our viewModel. By having this concept in mind, this will gives us huge benefit when it comes to have our application work in offline or inject datas to VM ( This was slight different that I mentioned ). That's why we have two classes confirming to this protocol 'HomeOfflineManager' and 'HomeDataProvider'. "HomeDataProvider" is getting NetworkInterface instance in its initializer to able to make network calls and "HomeOfflineManager" is responsible for providing data to our vm when app is offline. To simulate offline mode I made it possible by having a swift compiler flag named "OFFLINE_MODE". Here is the implementation in HomeCoordinator where I inject the data provider to the viewModel.
```swift
func toHome(animated: Bool = true) {
        var dataProvider: HomeServiceProtocol!
        #if OFFLINE_MODE
        dataProvider = HomeOfflineManager()
        #else
        dataProvider = HomeDataProvider(dataLoader: networkInterface)
        #endif
        let homeVM = HomeViewModel(dataProvider: dataProvider)
        let homeVC = HomeViewController(homeViewModel: homeVM)
        homeVC.coordinator = self
        navigationController.pushViewController(homeVC, animated: animated)
    }
```
Ofcourse instead of compiler flag I could have just check for internet conenction but for this code challenge I found this way more convenient ( which it wasn't at the end :D ). In order to run app in offline mode after running for first time open package.swift and change offline mode variable at the top of file. (Please clean twice after changing the variable, I added a note for it)
I don't want to go through all the implementation in the classes but this was the architecture of the code challenge. we can go thorugh more in deep in out demo interview but for now this readme should be enough to give you overal look of project and technical decesions. Thanks for reading and hope we talk soon :)
