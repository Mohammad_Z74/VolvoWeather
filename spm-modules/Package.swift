// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription
 
/// Use this variable to make Home page into offline mode ( IMPORTANT: YOU NEED TO CLEAN!! (CMD+SHIFT+K ) and build the project again to make this happen. ( After changing variable ))
let offlineMode: Bool = false
// ( IMPORTANT: YOU NEED TO CLEAN!! (CMD+SHIFT+K ) and build the project again to make this happen (After changing variable ))
// Unforunately spm has bug for handling this and sometimes I had to clean project more than once to able to enable/disable this compiler flag in module
let homeSettings: [SwiftSetting] =  offlineMode ? [.define("OFFLINE_MODE", .when(configuration: .debug))] : []

let package = Package(
    name: "spm-modules",
    platforms: [.iOS(.v16)],
    products: [
        .library(
            name: "Networking",
            targets: ["Networking"]),
        .library(
            name: "NetworkingInterface",
            targets: ["NetworkingInterface"]),
        .library(
            name: "Utilities",
            targets: ["Utilities"]),
        .library(
            name: "Home",
            targets: ["Home"]),
        .library(
            name: "Search",
            targets: ["Search"]),
        .library(
            name: "SearchInterface",
            targets: ["SearchInterface"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "Networking",
            dependencies: ["NetworkingInterface"]),
        .testTarget(
            name: "NetworkingTests",
            dependencies: ["Networking", "NetworkingInterface"]),
        .target(
            name: "NetworkingInterface",
            dependencies: []),
        .target(
            name: "Utilities",
            dependencies: []),
        .target(
            name: "Home",
            dependencies: ["NetworkingInterface", "Utilities", "SearchInterface"],
            swiftSettings: homeSettings
        ),
        .testTarget(
            name: "HomeTests",
            dependencies: ["Home"],
            resources: [.copy("stokcholm_forecast.json")]
        ),
        .target(
            name: "Search",
            dependencies: ["SearchInterface", "Utilities"],
            resources: [.copy("Model/SearchDefaultData.json")]
        ),
        .testTarget(
            name: "SearchTests",
            dependencies: ["Search", "SearchInterface", "NetworkingInterface"],
            resources: [.copy("MockProvider/search_stock.json")]
        ),
        .target(
            name: "SearchInterface",
            dependencies: [])
    ]
)
