//
//  HTTPMethod.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

public enum HTTPMethod: String, Equatable {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

