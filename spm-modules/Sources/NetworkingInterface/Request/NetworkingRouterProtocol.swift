//
//  NetworkingRouterProtocol.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Foundation

public protocol NetworkingRouterProtocol {
    var method: HTTPMethod { get }
    var path: String { get }
    var headerFields: [String: String] { get }
    var requestBody: HTTPBody? { get }
    var appId: String { get }
}

public extension NetworkingRouterProtocol {
    var appId: String {
        return "97c0d77922da7fdf0f9859f8eaffdb84"
    }
}
