//
//  HTTPClientProtocol.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Combine
import Foundation

public protocol HTTPClientProtocol: AnyObject {
    var baseURL: URL { get set }
    var defaultHeaders: [String: String] { get set }
}

