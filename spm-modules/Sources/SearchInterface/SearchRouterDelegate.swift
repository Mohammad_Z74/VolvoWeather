//
//  SearchRouterDelegate.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import Foundation

public protocol SearchRouterDelegate: AnyObject {
    func navigateToSearchWith(delegate: SearchDelegate, animated: Bool)
}
