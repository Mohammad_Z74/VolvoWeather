//
//  SearchDelegate.swift
//  
//
//  Created by Mo Zakizadeh on 2/20/23.
//

import CoreLocation

public protocol SearchDelegate: AnyObject {
    func updateHomeWith(loc: CLLocationCoordinate2D)
}
