//
//  HomeHeader.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import SwiftUI
import Utilities

struct HomeHeader: View {
    
    @ObservedObject var viewModel: HomeHeaderViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            HStack {
                #if OFFLINE_MODE
                Text("OFFLINE MODE").bold()
                    .fixedSize()
                #else
                Text("Now,").bold()
                    .fixedSize()
                Text(viewModel.currentDate)
                    .font(.caption)
                    .fixedSize()
                #endif
            }.foregroundColor(.white)
            HStack {
                Image(systemName: viewModel.weatherIcon.sfSymbolIconFilled)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(minWidth: 60, idealHeight: 50, maxHeight: 60)
                    .foregroundColor(.white)
                
                Text(viewModel.currentTemp)
                    .font(.system(size: 50))
                    .fontWeight(.heavy)
                    .foregroundColor(.white)
                    .fixedSize()
            }
            Text(viewModel.weatherDesc)
                .foregroundColor(.white)
                .fontWeight(.heavy)
                .fixedSize()
        }
    }
}

final class HomeHeaderViewModel: ObservableObject {
    
    @Published var weatherData: WeeklyForecast?
    
    init(weatherData: WeeklyForecast?) {
        self.weatherData = weatherData
    }
    
    var currentDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let tDate = dateFormatter.date(from: weatherData?.list[safe: 0]?.dateTxt ?? "") ?? Date()
        let tFormatter = DateFormatter()
        tFormatter.dateFormat = "dd MMMM yyyy HH:mm"
        return tFormatter.string(from: tDate)
    }
    
    var currentTemp: String {
        if let temp = weatherData?.list[safe: 0]?.main.temp {
            return String(temp) + "°C"
        } else {
            return "" + "°C"
        }
    }
    
    var weatherDesc: String {
        let weatherDesc = weatherData?.list[safe: 0]?.extraData[safe: 0]?.description ?? ""
        return weatherDesc.capitalized
    }
    
    var weatherIcon: WeatherIcon {
        let icon = WeatherIcon(id: weatherData?.list[safe: 0]?.extraData[safe: 0]?.id)
        return icon
    }
}
