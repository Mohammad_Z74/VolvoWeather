//
//  SwiftUIView.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import SwiftUI
import Utilities

struct WeatherTableViewCell: View {
    
    let weather: Weather
    
    private var weatherDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let tDate = dateFormatter.date(from: weather.dateTxt) ?? Date()
        return tDate
    }
    
    private var weatherDay: String {
        let tFormatter = DateFormatter()
        tFormatter.dateFormat = "EEEE h a"
        return tFormatter.string(from: weatherDate)
    }
    
    private var weatherFormattedDate: String {
        let tFormatter = DateFormatter()
        tFormatter.dateFormat = "MMM d, yyyy"
        return tFormatter.string(from: weatherDate)
    }
    
    private var weatherTemp: String {
        String(weather.main.temp) + "°" + "C"
    }
    
    var weatherIcon: WeatherIcon {
        let icon = WeatherIcon(id: weather.extraData[safe: 0]?.id)
        return icon
    }
    
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text(weatherDay)
                        .font(.headline)
                        .foregroundColor(Color("TitleGrey"))
                    Text(weatherFormattedDate)
                        .font(.subheadline)
                        .foregroundColor(Color("SubtitleGrey"))
                }
                Spacer()
                HStack() {
                    Image(systemName: weatherIcon.sfSymbolIconName)
                        .resizable()
                        .fixedSize()
                        .foregroundColor(.blue)
                        .padding(15)
                        .background(Color("IconBackground"))
                        .clipShape(Circle())
                    Text(weatherTemp)
                        .font(.body)
                        .bold()
                        .foregroundColor(Color("TitleGrey"))
                        .fixedSize()
                        .frame(maxWidth: 60)
                }
            }.background(.clear)
        }
    }
}
