//
//  HomeViewController.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Combine
import SwiftUI
import UIKit

final class HomeViewController: UIViewController {
    
    //MARK: subviews
    
    private lazy var currentTemp: UILabel = {
        let label = UILabel()
        self.view.addSubview(label)
        label.textColor = .systemGray2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        return label
    }()
    
    private lazy var locIcon: UIImageView = {
        let icon: UIImageView = UIImageView()
        let locPinImage: UIImage = UIImage(named: "locpin")!
        icon.image = locPinImage
        self.view.addSubview(icon)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 28).isActive = true
        icon.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        return icon
    }()
    
    private lazy var currentLocLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        self.view.addSubview(label)
        label.text = "--"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: locIcon.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: locIcon.trailingAnchor, constant: 4).isActive = true
        return label
    }()

    /// This is offline indicator when OFFLINE_MODE is on
    private lazy var searchBtn: UIButton = {
        let searchBtn: UIButton = UIButton()
#if OFFLINE_MODE
        let offlineImage: UIImage = UIImage(systemName: "icloud.slash.fill")!.withTintColor(.white, renderingMode: .alwaysOriginal)
        searchBtn.setImage(offlineImage, for: .normal)
#else
        let searchImage: UIImage = UIImage(named: "Search")!
        searchBtn.setImage(searchImage, for: .normal)
        searchBtn.addTarget(self, action: #selector(toSearch), for: .touchUpInside)
#endif
        self.view.addSubview(searchBtn)
        searchBtn.translatesAutoresizingMaskIntoConstraints = false
        searchBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -28).isActive = true
        searchBtn.centerYAnchor.constraint(equalTo: locIcon.centerYAnchor).isActive = true
        return searchBtn
    }()
    
    private lazy var headerView: UIHostingController = {
        let hostController: UIHostingController = UIHostingController(rootView: HomeHeader(viewModel: viewModel.homeHeaderViewModel))
        self.addChild(hostController)
        self.view.addSubview(hostController.view)
        hostController.view.backgroundColor = .clear
        hostController.view.translatesAutoresizingMaskIntoConstraints = false
        hostController.view.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        hostController.view.topAnchor.constraint(equalTo: searchBtn.bottomAnchor, constant: 50).isActive = true
        return hostController
    }()
    
    private lazy var footerView: UIView = {
        let footerView: UIView = UIView()
        footerView.backgroundColor = UIColor(named: "VLVWhite")
        self.view.addSubview(footerView)
        footerView.translatesAutoresizingMaskIntoConstraints = false
        return footerView
    }()
    
    private lazy var tableViewContainerView: UIView = {
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor(named: "VLVBlue")
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10
        footerView.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 28).isActive = true
        containerView.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -28).isActive = true
        containerView.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 34).isActive = true
        containerView.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -28).isActive = true
        return containerView
    }()
    
    private lazy var forecastTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        viewModel.forecastTableViewDataSource = ForecastWeatherDataSource(tableView: tableView)
        tableView.delegate = viewModel.forecastTableViewDataSource
        tableView.dataSource = viewModel.forecastTableViewDataSource
        tableView.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: -30, right: 0)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ForecastWeatherDataSource.cellReuseID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = 70
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        self.tableViewContainerView.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: tableViewContainerView.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: tableViewContainerView.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: tableViewContainerView.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: tableViewContainerView.trailingAnchor).isActive = true
        return tableView
    }()
    
    
    // MARK: Properties
    
    weak var coordinator: HomeCoordinator?
    
    let viewModel: HomeViewModel
    
    private var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    // MARK: Initializers
    
    init(homeViewModel: HomeViewModel) {
        self.viewModel = homeViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: ViewCycle
    
    override func viewDidLoad() {
        addSubViews()
        setupView()
        bindData()
        viewModel.askForUserLocation()
        loadData()
    }
    
    override func viewDidLayoutSubviews() {
        resizeFooterView()
    }
    
    private func addSubViews() {
        self.view.addSubview(currentTemp)
        self.view.addSubview(locIcon)
        self.view.addSubview(currentLocLabel)
        self.view.addSubview(searchBtn)
        self.addChild(headerView)
        self.view.addSubview(footerView)
        footerView.addSubview(tableViewContainerView)
        tableViewContainerView.addSubview(forecastTableView)
    }
    
    private func resizeFooterView() {
        // Resize footerView to 65% of parent view
        footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        let height: CGFloat = self.view.frame.size.height * 0.65
        footerView.heightAnchor.constraint(equalToConstant: height).isActive = true
        // Apply corner radius
        footerView.clipsToBounds = true
        footerView.layer.cornerCurve = .continuous
        footerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        footerView.layer.cornerRadius = 30
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor(named: "BackgroundBlue")
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: Data loading
    
    private func loadData() {
        if let userLocation = viewModel.userLocation {
            viewModel.loadWeatherData(for: userLocation)
        } else {
            viewModel.loadWeatherData(for: viewModel.defaultLocation)
        }
    }
    
    // MARK: Bind
    
    func bindData() {
        viewModel
            .$weatherData
            .compactMap{$0}
            .receive(on: DispatchQueue.main)
            .sink { [weak self] weatherData in
                guard let self else { return }
                self.currentLocLabel.text = weatherData.city.name + ", " + weatherData.city.country
            }.store(in: &disposables)
    }
    
    // MARK: Actions
    
    @objc func toSearch() {
        coordinator?.toSearch(searchDelegate: viewModel)
    }
}
