//
//  HomeCoordinator.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import NetworkingInterface
import SearchInterface
import UIKit
import Utilities

public final class HomeCoordinator: Coordinator {
    
    public weak var parentCoordinator: Coordinator?
    weak var searchRouterDelegate: SearchRouterDelegate?
    
    public var childCoordinators: [Coordinator] = []
    
    public var navigationController: UINavigationController
    
    let networkInterface: NetworkingInterface
    
    public init(navigationController: UINavigationController, networkInterface: NetworkingInterface, searchRouterDelegate: SearchRouterDelegate) {
        self.navigationController = navigationController
        self.networkInterface = networkInterface
        self.searchRouterDelegate = searchRouterDelegate
    }
    
    public func start(animated: Bool = true) {
        self.toHome()
    }
    
    func toHome(animated: Bool = true) {
        var dataProvider: HomeServiceProtocol!
        #if OFFLINE_MODE
        dataProvider = HomeOfflineManager()
        #else
        dataProvider = HomeDataProvider(dataLoader: networkInterface)
        #endif
        let homeVM = HomeViewModel(dataProvider: dataProvider)
        let homeVC = HomeViewController(homeViewModel: homeVM)
        homeVC.coordinator = self
        navigationController.pushViewController(homeVC, animated: animated)
    }
    
    func toSearch(searchDelegate: SearchDelegate, animated: Bool = true) {
        searchRouterDelegate?.navigateToSearchWith(delegate: searchDelegate, animated: animated)
    }
}
