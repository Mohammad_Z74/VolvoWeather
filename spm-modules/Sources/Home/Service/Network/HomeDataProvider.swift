//
//  HomeDataProvider.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import CoreLocation
import Foundation
import NetworkingInterface

final class HomeDataProvider: HomeServiceProtocol {
    
    let dataProvider: NetworkingInterface
    
    init(dataLoader: NetworkingInterface) {
        self.dataProvider = dataLoader
    }
    
    func loadWeather(for location: CLLocationCoordinate2D) async throws -> WeeklyForecast {
        let homeNetworkRouter: HomeServiceRouter = HomeServiceRouter(location: location, city: nil, numberOfDays: 15)
        let weatherData = try await dataProvider.fetchData(with: homeNetworkRouter, for: WeeklyForecast.self)
        return weatherData
    }
    
    func loadWeather(for city: String) async throws -> WeeklyForecast {
        let homeNetworkRouter: HomeServiceRouter = HomeServiceRouter(location: nil, city: city, numberOfDays: 15)
        let weatherData = try await dataProvider.fetchData(with: homeNetworkRouter, for: WeeklyForecast.self)
        return weatherData
    }
}
