//
//  HomeServiceRouter.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import CoreLocation
import NetworkingInterface

final class HomeServiceRouter: NetworkingRouterProtocol {
    
    let location: CLLocationCoordinate2D?
    let city: String?
    let numberOfDays: Int
    
    var method: HTTPMethod {
        .get
    }
    
    var path: String {
        "forecast"
    }
    
    var headerFields: [String : String] = ["Accept-Encoding": "gzip, deflate", "Accept": "application/json"]
    
    var requestBody: HTTPBody? {
        var body = HTTPBody(content: Data())
        if let city {
            body.params = [
                URLQueryItem(name: "q", value: city),
                URLQueryItem(name: "cnt", value: "\(numberOfDays)"),
                URLQueryItem(name: "units", value: "metric"),
                URLQueryItem(name: "appid", value: appId)
            ]
        } else if let location {
            body.params = [
                URLQueryItem(name: "lat", value: "\(location.latitude)"),
                URLQueryItem(name: "lon", value: "\(location.longitude)"),
                URLQueryItem(name: "cnt", value: "\(numberOfDays)"),
                URLQueryItem(name: "units", value: "metric"),
                URLQueryItem(name: "appid", value: appId)
            ]
        }
        return body
    }
    
    init(location: CLLocationCoordinate2D?, city: String?, numberOfDays: Int) {
        self.location = location
        self.city = city
        self.numberOfDays = min(numberOfDays, 30)
    }
}
