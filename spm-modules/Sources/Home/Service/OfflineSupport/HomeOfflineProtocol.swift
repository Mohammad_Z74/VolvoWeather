//
//  File.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Foundation

protocol HomeOfflineProtocol {
    func saveData(with weatherData: WeeklyForecast) async
}
