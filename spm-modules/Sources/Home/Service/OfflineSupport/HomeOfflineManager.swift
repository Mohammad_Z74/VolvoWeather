//
//  HomeOfflineManager.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import CoreLocation
import Foundation

actor HomeOfflineManager: HomeServiceProtocol, HomeOfflineProtocol {
    
    init() {}
    
    func loadWeather(for location: CLLocationCoordinate2D) async throws -> WeeklyForecast {
        let weatherData = try loadData()
        return weatherData
    }
    
    func loadWeather(for city: String) async throws -> WeeklyForecast {
        let weatherData = try loadData()
        return weatherData
    }
    
    func saveData(with weatherData: WeeklyForecast) {
        try? self.save(weatherData: weatherData)
    }
    
    private func save(weatherData: Encodable, toFilename filename: String = "WeatherForecast") throws  {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent(filename)
            fileURL = fileURL.appendingPathExtension("json")
            let data = try JSONEncoder().encode(weatherData)
            try data.write(to: fileURL, options: [.atomicWrite])
        } else {
            throw OfflineError.saveError
        }
    }
    
    func loadData(withFilename filename: String = "WeatherForecast") throws -> WeeklyForecast {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent(filename)
            fileURL = fileURL.appendingPathExtension("json")
            let data = try Data(contentsOf: fileURL)
            let weatherForecast = try JSONDecoder().decode(WeeklyForecast.self, from: data)
            return weatherForecast
        }
        throw OfflineError.savedDataNotFound
    }
}

enum OfflineError: Error {
    case savedDataNotFound
    case saveError
}
