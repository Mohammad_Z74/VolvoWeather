//
//  HomeServiceProtocol.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import CoreLocation

protocol HomeServiceProtocol {
    func loadWeather(for location: CLLocationCoordinate2D) async throws -> WeeklyForecast
    func loadWeather(for city: String) async throws -> WeeklyForecast
}
