//
//  WeeklyForecast.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Foundation

// MARK: - WeeklyForecast

struct WeeklyForecast: Codable {
    let list: [Weather]
    let city: City
}
// MARK: - City
struct City: Codable {
    let name: String
    let country: String
}
// MARK: - List
struct Weather: Codable {
    let date: Int
    let main: WeatherData
    let extraData: [Info]
    let dateTxt: String

    enum CodingKeys: String, CodingKey {
        case main
        case extraData = "weather"
        case dateTxt = "dt_txt"
        case date = "dt"
    }
}

// MARK: - Main
struct WeatherData: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, seaLevel, grndLevel, humidity: Int

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
    }
}

// MARK: - Weather
struct Info: Codable {
    let id: Int
    let main, description, icon: String
}
