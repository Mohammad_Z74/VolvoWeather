//
//  ForecastWeatherDataSource.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import SwiftUI
import UIKit

final class ForecastWeatherDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private(set) var weathers: [Weather] = []
    
    public static let cellReuseID = "cell"
    
    private let tableView: UITableView
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.cellReuseID, for: indexPath)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        cell.contentConfiguration = UIHostingConfiguration(content: {
            WeatherTableViewCell(weather: weathers[indexPath.row])
        })
        return cell
    }
    
    func updateForecast(with weathers: [Weather]) {
        self.weathers = weathers
        self.tableView.reloadData()
    }
}
