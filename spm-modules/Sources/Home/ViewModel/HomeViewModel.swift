//
//  HomeViewModel.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Combine
import CoreLocation
import Foundation
import NetworkingInterface
import SearchInterface

final class HomeViewModel: NSObject, SearchDelegate, ObservableObject {

    // MARK: Dependencies
    private let locationManager = CLLocationManager()
    private let dataProvider: HomeServiceProtocol
    private let offlineManager: HomeOfflineProtocol = HomeOfflineManager()
    
    // MARK: Properties
    var userLocation: CLLocationCoordinate2D? {
        didSet {
            guard let userLocation else { return }
            self.loadWeatherData(for: userLocation)
        }
    }
    var forecastTableViewDataSource: ForecastWeatherDataSource?
    
    @Published var weatherData: WeeklyForecast?
    @Published var homeHeaderViewModel: HomeHeaderViewModel = HomeHeaderViewModel(weatherData: nil)
    
    /// Default is set to Stockholm
    let defaultLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 59.3293, longitude: 18.0686)
    
    // MARK: Initializers
    
    init(dataProvider: HomeServiceProtocol) {
        self.dataProvider = dataProvider
        super.init()
        locationManager.delegate = self
    }
    
    // MARK: Methods
    
    func askForUserLocation() {
        locationManager.requestLocation()
    }
    
    func loadWeatherData(for location: CLLocationCoordinate2D) {
        Task {
            do {
                let weatherData = try await dataProvider.loadWeather(for: location)
                #if !OFFLINE_MODE
                Task(priority: .background) {
                    await self.offlineManager.saveData(with: weatherData)
                }
                #endif
                await MainActor.run(body: {
                    self.weatherData = weatherData
                    self.homeHeaderViewModel.weatherData = weatherData
                    self.forecastTableViewDataSource?.updateForecast(with: weatherData.list)
                })
            } catch let error as HTTPResponseError {
                print(error)
            }
        }
    }
    
    func updateHomeWith(loc: CLLocationCoordinate2D) {
        self.loadWeatherData(for: loc)
    }
    
    deinit {
        print("Removed \(self) from memory")
    }
}

// MARK: LocationManager

extension HomeViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.userLocation = location.coordinate
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) { }
}
