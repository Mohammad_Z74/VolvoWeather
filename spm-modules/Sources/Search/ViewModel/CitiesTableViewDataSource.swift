//
//  CitiesTableViewDataSource.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import SwiftUI
import UIKit

final class CitiesTableViewDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    @Published private(set) var weathers: [WeatherData] = []
    
    public static let cellReuseID = "cell"
    
    private let tableView: UITableView
    var didSelectCityWith: ((_ coord: Coord) -> Void)?
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.cellReuseID, for: indexPath)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        cell.contentConfiguration = UIHostingConfiguration(content: {
            SearchTableViewCell(weather: weathers[indexPath.row], width: tableView.frame.width)
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectCityWith?(weathers[indexPath.row].coord)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func updateForecast(with weathers: [WeatherData]) {
        self.weathers = weathers
        self.tableView.isHidden = false
        self.tableView.reloadData()
    }
    
    deinit {
        print("Removed \(self) from memory")
    }
}
