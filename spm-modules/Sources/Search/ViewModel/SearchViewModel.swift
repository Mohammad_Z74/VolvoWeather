//
//  SearchViewModel.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import Combine
import CoreLocation
import SearchInterface

final class SearchViewModel {
    
    var citiesTableViewDataSource: CitiesTableViewDataSource?
    
    private let searchDelegate: SearchDelegate
    private let dataProvider: SearchServiceProtocol
    var showErrorLabel: ((Bool) -> Void)?
    
    init(searchDelegate: SearchDelegate, dataLoader: SearchServiceProtocol) {
        self.searchDelegate = searchDelegate
        self.dataProvider = dataLoader
    }
    
    private var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    func searchForCity(city: String) {
        Task {
            do {
                let weatherData = try await self.dataProvider.search(for: city)
                await MainActor.run {
                    self.citiesTableViewDataSource?.updateForecast(with: [weatherData])
                    showErrorLabel?(false)
                }
            } catch {
                await MainActor.run {
                    showErrorLabel?(true)
                }
            }
        }
    }
    
    func searchForCityPublisherCall(city: String) {
        dataProvider
            .search(for: city)
            .receive(on: RunLoop.main)
            .sink { completion in
                switch completion {
                case .failure(_):
                    self.showErrorLabel?(true)
                default:
                    break
                }
            } receiveValue: { [weak self] weatherData in
                guard let self else { return }
                self.citiesTableViewDataSource?.updateForecast(with: [weatherData])
                self.showErrorLabel?(false)
            }.store(in: &disposables)
    }
    
    func loadDefaultCities() {
        do {
            let weathers = try dataProvider.loadDefaultCities()
            self.citiesTableViewDataSource?.updateForecast(with: weathers)
        } catch {
            print(error)
        }
    }
    
    func userSelected(loc: CLLocationCoordinate2D) {
        searchDelegate.updateHomeWith(loc: loc)
    }
    
    deinit {
        print("Removed \(self) from memory")
    }
}
