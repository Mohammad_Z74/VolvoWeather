//
//  SearchServiceRouter.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Foundation
import NetworkingInterface

final class SearchServiceRouter: NetworkingRouterProtocol {
    
    let city: String
    
    var method: HTTPMethod {
        .get
    }
    
    var path: String {
        "weather"
    }
    
    var headerFields: [String : String] = ["Accept-Encoding": "gzip, deflate", "Accept": "application/json"]
    
    var requestBody: HTTPBody? {
        var body = HTTPBody(content: Data())
        body.params = [
            URLQueryItem(name: "q", value: city),
            URLQueryItem(name: "units", value: "metric"),
            URLQueryItem(name: "appid", value: appId)
        ]
        return body
    }
    
    init(city: String) {
        self.city = city
    }
}
