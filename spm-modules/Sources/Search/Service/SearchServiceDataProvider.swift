//
//  SearchServiceDataProvider.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Combine
import Foundation
import NetworkingInterface

final class SearchServiceDataProvider: SearchServiceProtocol {
    
    enum DefaultCitiesLoadError: Error {
        case loadError
    }
    
    let dataProvider: NetworkingInterface
    
    init(dataLoader: NetworkingInterface) {
        self.dataProvider = dataLoader
    }
    
    func search(for city: String) async throws -> WeatherData {
        let searchRouter: SearchServiceRouter = SearchServiceRouter(city: city)
        let weatherData = try await dataProvider.fetchData(with: searchRouter, for: WeatherData.self)
        return weatherData
    }
    
    func search(for city: String) -> AnyPublisher<WeatherData, HTTPResponseError> {
        let searchRouter: SearchServiceRouter = SearchServiceRouter(city: city)
        return dataProvider
            .fetchDataPublisher(with: searchRouter, for: WeatherData.self)
    }
    
    func loadDefaultCities() throws -> [WeatherData] {
        guard let fileUrl = Bundle.module.url(forResource: "SearchDefaultData", withExtension: "json") else {
            throw DefaultCitiesLoadError.loadError
        }

        let jsonData = try Data(contentsOf: fileUrl)
        let weatherData = try JSONDecoder().decode([WeatherData].self, from: jsonData)

        return weatherData
    }
}
