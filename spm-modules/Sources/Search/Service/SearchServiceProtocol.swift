//
//  SearchServiceProtocol.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Combine
import NetworkingInterface
import Foundation

protocol SearchServiceProtocol {
    func search(for city: String) async throws -> WeatherData
    func search(for city: String) -> AnyPublisher<WeatherData, HTTPResponseError>
    func loadDefaultCities() throws -> [WeatherData]
}
