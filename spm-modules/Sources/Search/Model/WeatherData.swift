//
//  WeatherData.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Foundation

// MARK: - WeatherData

struct WeatherData: Decodable {
    let coord: Coord
    let city: City
    let weather: [Weather]
    let name: String
    let main: Main
    enum CodingKeys: String, CodingKey {
        case coord
        case city = "sys"
        case weather
        case name
        case main
    }
}

// MARK: - Coord
struct Coord: Decodable {
    let lon, lat: Double
}

// MARK: - Main
struct Main: Codable {
    let tempMax: Double
    let humidity: Int
    let feelsLike, tempMin, temp: Double
    let pressure: Int

    enum CodingKeys: String, CodingKey {
        case tempMax = "temp_max"
        case humidity
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case temp, pressure
    }
}

// MARK: - Sys
struct City: Decodable {
    let id: Int
    let country: String
    let sunset, type, sunrise: Int
}

// MARK: - Weather
struct Weather: Decodable {
    let id: Int
    let main, icon, description: String
}
