//
//  SearchCoordinator.swift
//  
//
//  Created by Mo Zakizadeh on 2/20/23.
//

import NetworkingInterface
import SearchInterface
import Utilities
import UIKit

public final class SearchCoordinator: Coordinator {
    
    public weak var parentCoordinator: Coordinator?
    
    public var childCoordinators: [Coordinator] = []
    
    public var navigationController: UINavigationController
    
    let networkInterface: NetworkingInterface
    
    public init(navigationController: UINavigationController, networkInterface: NetworkingInterface) {
        self.navigationController = navigationController
        self.networkInterface = networkInterface
    }
    
    public func start(animated: Bool = true) {}
    
    public func toSearch(searchDelegate: SearchDelegate, animated: Bool = true) {
        let searchViewModel: SearchViewModel = SearchViewModel(searchDelegate: searchDelegate, dataLoader: SearchServiceDataProvider(dataLoader: networkInterface))
        let searchViewController: SearchViewController = SearchViewController(searchViewModel: searchViewModel)
        searchViewController.coordinator = self
        navigationController.pushViewController(searchViewController, animated: animated)
    }
    
    func returnToHome() {
        navigationController.popViewController(animated: true)
    }
}
