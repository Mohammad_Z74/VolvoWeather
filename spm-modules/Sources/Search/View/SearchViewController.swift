//
//  SearchViewController.swift
//  
//
//  Created by Mo Zakizadeh on 2/22/23.
//

import CoreLocation
import Combine
import UIKit
import Utilities

final class SearchViewController: UIViewController {
    
    // MARK: subviews
    
    private lazy var closeBtn: UIButton = {
        let closeBtn: UIButton = UIButton()
        let closeImage: UIImage = UIImage(systemName: "xmark")!.withTintColor(.white, renderingMode: .alwaysOriginal)
        closeBtn.setImage(closeImage, for: .normal)
        closeBtn.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
        self.view.addSubview(closeBtn)
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 28).isActive = true
        closeBtn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        closeBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        closeBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        return closeBtn
    }()
    
    private lazy var chooseCityLabel: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        label.text = "Choose a city"
        label.textAlignment = .center
        label.font = .boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        label.topAnchor.constraint(equalTo: closeBtn.bottomAnchor, constant: 16).isActive = true
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        return label
    }()
    
    private lazy var selectCityLabelDesc: UILabel = {
        let label: UILabel = UILabel()
        label.textColor = .white
        label.text = "Select or search a city to see the weather"
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14)
        self.view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: chooseCityLabel.bottomAnchor, constant: 8).isActive = true
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        return label
    }()
    
    private lazy var searchTextfieldContainerView: UIView = {
        let view: UIView = UIView()
        footerView.addSubview(view)
        view.backgroundColor = UIColor(named: "SearchBG")
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 28).isActive = true
        view.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -28).isActive = true
        view.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 24).isActive = true
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let searchImgView: UIImageView = UIImageView()
        let searchImage: UIImage = UIImage(systemName: "magnifyingglass")!.withTintColor(UIColor(named: "SubtitleGrey")!, renderingMode: .alwaysOriginal)
        searchImgView.image = searchImage
        searchImgView.contentMode = .scaleAspectFit
        view.addSubview(searchImgView)
        searchImgView.translatesAutoresizingMaskIntoConstraints = false
        searchImgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        searchImgView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        searchImgView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        return view
    }()
    
    private lazy var searchTextfield: UITextField = {
        let searchTF: UITextField = UITextField()
        searchTextfieldContainerView.addSubview(searchTF)
        searchTF.translatesAutoresizingMaskIntoConstraints = false
        searchTF.backgroundColor = .clear
        searchTF.placeholder = "Search a City"
        searchTF.delegate = self
        searchTF.accessibilityIdentifier = "SearchCityTextField"
        searchTF.leadingAnchor.constraint(equalTo: searchTextfieldContainerView.leadingAnchor, constant: 40).isActive = true
        searchTF.trailingAnchor.constraint(equalTo: searchTextfieldContainerView.trailingAnchor).isActive = true
        searchTF.topAnchor.constraint(equalTo: searchTextfieldContainerView.topAnchor).isActive = true
        searchTF.bottomAnchor.constraint(equalTo: searchTextfieldContainerView.bottomAnchor).isActive = true
        return searchTF
    }()
    
    private lazy var errorLabel: UILabel = {
        let label: UILabel = UILabel()
        searchTextfieldContainerView.addSubview(label)
        label.text = "Oops, no city found"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.isHidden = true
        label.centerXAnchor.constraint(equalTo: footerView.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
        return label
    }()
    
    private lazy var footerView: UIView = {
        let footerView: UIView = UIView()
        footerView.backgroundColor = UIColor(named: "VLVWhite")
        self.view.addSubview(footerView)
        footerView.translatesAutoresizingMaskIntoConstraints = false
        return footerView
    }()
    
    private lazy var citiesTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        viewModel.citiesTableViewDataSource = CitiesTableViewDataSource(tableView: tableView)
        viewModel.citiesTableViewDataSource?.didSelectCityWith = { [weak self] coordinate in
            guard let self else { return }
            self.viewModel.userSelected(loc: CLLocationCoordinate2D(latitude: coordinate.lat, longitude: coordinate.lon))
            self.coordinator?.returnToHome()
        }
        tableView.delegate = viewModel.citiesTableViewDataSource
        tableView.dataSource = viewModel.citiesTableViewDataSource
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CitiesTableViewDataSource.cellReuseID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = 70
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        
        self.footerView.addSubview(tableView)
        tableView.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 28).isActive = true
        tableView.topAnchor.constraint(equalTo: searchTextfieldContainerView.bottomAnchor, constant: -4).isActive = true
        tableView.bottomAnchor.constraint(equalTo: footerView.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -28).isActive = true
        return tableView
    }()
    
    // MARK: Properties
    
    weak var coordinator: SearchCoordinator?
    
    private var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
    
    let viewModel: SearchViewModel
    
    init(searchViewModel: SearchViewModel) {
        self.viewModel = searchViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: ViewCycle
    
    override func viewDidLayoutSubviews() {
        resizeFooterView()
    }
    
    override func viewDidLoad() {
        setupView()
        addSubViews()
        bind()
        viewModel.loadDefaultCities()
    }
    
    private func addSubViews() {
        self.view.addSubview(closeBtn)
        self.view.addSubview(chooseCityLabel)
        self.view.addSubview(selectCityLabelDesc)
        self.view.addSubview(footerView)
        footerView.addSubview(searchTextfieldContainerView)
        footerView.addSubview(errorLabel)
        searchTextfieldContainerView.addSubview(searchTextfield)
        footerView.addSubview(citiesTableView)
    }
    
    private func resizeFooterView() {
        // Resize footerView to 70% of parent view
        footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        footerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        footerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        let height: CGFloat = self.view.frame.size.height * 0.7
        footerView.heightAnchor.constraint(equalToConstant: height).isActive = true
        // Apply corner radius
        footerView.clipsToBounds = true
        footerView.layer.cornerCurve = .continuous
        footerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        footerView.layer.cornerRadius = 30
    }
    
    private func setupView() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = UIColor(named: "BackgroundBlue")
    }
    
    // MARK: Bind
    
    func bind() {
        self.searchTextfield
            .textPublisher
            .compactMap {$0}
            .debounce(for: 0.5, scheduler: DispatchQueue.main)
            .sink { [weak self] phrase in
                guard let self else { return }
                if phrase.isEmpty {
                    self.viewModel.loadDefaultCities()
                } else {
                    self.viewModel.searchForCityPublisherCall(city: phrase)
                    // self.viewModel.searchForCity(city: phrase)
                }
            }.store(in: &disposables)
        
        self.viewModel
            .showErrorLabel = { [weak self] isHidden in
                guard let self else { return }
                self.errorLabel.isHidden = !isHidden
                self.citiesTableView.isHidden = isHidden
            }
    }
    
    // MARK: actions
    
    @objc func closeSearch() {
        self.coordinator?.returnToHome()
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
