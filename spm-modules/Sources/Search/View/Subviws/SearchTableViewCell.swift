//
//  SearchTableViewCell.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Utilities
import SwiftUI

struct SearchTableViewCell: View {
    
    let weather: WeatherData
    let width: CGFloat
    
    private var weatherTemp: String {
        let temp = String(format: "%.2f", weather.main.temp)
        return temp + "°" + "C"
    }
    
    var weatherIcon: WeatherIcon {
        let icon = WeatherIcon(id: weather.weather[safe: 0]?.id)
        return icon
    }
    
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color("VLVBlue"))
                .frame(width: width, height: 70)
            HStack {
                VStack(alignment: .leading) {
                    Text(weather.name)
                        .font(.headline)
                        .foregroundColor(Color("TitleGrey"))
                    Text(weather.city.country)
                        .font(.subheadline)
                        .foregroundColor(Color("SubtitleGrey"))
                }.padding(.leading)
                Spacer()
                HStack() {
                    Image(systemName: weatherIcon.sfSymbolIconName)
                        .resizable()
                        .fixedSize()
                        .foregroundColor(.blue)
                        .padding(15)
                        .background(Color("IconBackground"))
                        .clipShape(Circle())
                    Text(weatherTemp)
                        .font(.body)
                        .bold()
                        .foregroundColor(Color("TitleGrey"))
                        .fixedSize()
                        .frame(maxWidth: 60)
                        .padding(.trailing)
                }
            }
        }
    }
}
