//
//  HTTPLoggerProtocol.swift
//  
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Foundation

public protocol HTTPLoggerProtocol {
    /// Use this function to debug network calls
    /// - Parameters:
    ///   - request: Request that executed
    ///   - response: Response from server
    ///   - error: Errors that might have thrown
    ///   - executionTime: Request executionTime
    func log(for request: HTTPRequest, response: HTTPResponse?, error: Error?, executionTime: TimeInterval)
}
