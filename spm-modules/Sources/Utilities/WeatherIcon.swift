//
//  WeatherIcon.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

public enum WeatherIcon {
    case thunderstorm
    case drizzle
    case rain
    case snow
    case wind
    case clear
    case cloud
    
    public init(id: Int?) {
        guard let id else {
            self = .cloud
            return
        }
        switch id {
        case 200..<300:
            self = .thunderstorm
        case 300..<400:
            self = .drizzle
        case 500..<600:
            self = .rain
        case 600..<700:
            self = .snow
        case 700..<800:
            self = .wind
        case 800:
            self = .clear
        case 801..<805:
            self = .cloud
        default:
            self = .cloud
        }
    }
    
    public var sfSymbolIconName: String {
        switch self {
        case .thunderstorm:
            return "cloud.bolt"
        case .drizzle:
            return "cloud.drizzle"
        case .rain:
            return "cloud.rain"
        case .snow:
            return "cloud.snow"
        case .wind:
            return "wind"
        case .clear:
            return "sun.max"
        case .cloud:
            return "cloud"
        }
    }
    
    public var sfSymbolIconCircle: String {
        return sfSymbolIconName + ".circle"
    }
    
    public var sfSymbolIconFilled: String {
        return sfSymbolIconName + ".fill"
    }
}

