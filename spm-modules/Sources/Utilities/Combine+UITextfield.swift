//
//  Combine+UITextfield.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Combine
import UIKit

public extension UITextField {

    var textPublisher: AnyPublisher<String?, Never> {
        NotificationCenter.default.publisher(
            for: UITextField.textDidChangeNotification,
            object: self
        )
        .map { ($0.object as? UITextField)?.text }
        .eraseToAnyPublisher()
    }

}
