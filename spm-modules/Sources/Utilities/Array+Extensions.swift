//
//  Array+Extensions.swift
//  
//
//  Created by Mo Zakizadeh on 2/23/23.
//

import Foundation

public extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
