//
//  HomeOfflineManagerTests.swift
//  
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import XCTest
@testable import Home

final class HomeOfflineManagerTests: XCTestCase {
    
    var sut: HomeOfflineManager!
    private var mockManager: HomeServiceDataMock = HomeServiceDataMock()
    
    override func setUp() {
        sut = HomeOfflineManager()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testSaveAndLoadData() async {
        // given
        let weatherData: WeeklyForecast? = try? await mockManager.loadWeather(for: "anycity")
        XCTAssertNotNil(weatherData)
        // when
        guard let weatherData else {
            XCTFail("Couldn't load data")
            return
        }
        await sut.saveData(with:  weatherData)
        // then
        let loadedData: WeeklyForecast? = try? await sut.loadData()
        XCTAssertNotNil(loadedData)
    }

}
