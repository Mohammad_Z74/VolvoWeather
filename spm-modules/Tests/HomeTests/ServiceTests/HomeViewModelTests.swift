//
//  HomeViewModelTests.swift
//  
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import Combine
import CoreLocation
import XCTest
@testable import Home

final class HomeViewModelTests: XCTestCase {
    
    var sut: HomeViewModel!
    
    override func setUp() {
        let mockService: HomeServiceProtocol = HomeServiceDataMock()
        sut = HomeViewModel(dataProvider: mockService)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testDataLoad() {
        // given
        let expectation = XCTestExpectation(description: "loaddata")
        var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
        // when
        sut.loadWeatherData(for: CLLocationCoordinate2D())
        sut.$weatherData
            .compactMap {$0}
            .sink { weather in
                expectation.fulfill()
            }.store(in: &disposables)
        
        // then
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testHomeUpdate() {
        // given
        let expectation = XCTestExpectation(description: "loaddata")
        var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
        // when
        sut.updateHomeWith(loc: CLLocationCoordinate2D())
        sut.$weatherData
            .compactMap {$0}
            .sink { weather in
                expectation.fulfill()
            }.store(in: &disposables)
        
        // then
        wait(for: [expectation], timeout: 0.5)
    }
}
