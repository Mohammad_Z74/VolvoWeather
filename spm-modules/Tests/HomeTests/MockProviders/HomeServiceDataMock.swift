//
//  HomeServiceDataMock.swift
//  
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import CoreLocation
import Foundation
@testable import Home

final class HomeServiceDataMock: HomeServiceProtocol {
    
    enum MockError: Error {
        case mockLoadError
    }
    
    private func loadMockedData() throws -> WeeklyForecast {
        guard let fileUrl = Bundle.module.url(forResource: "stokcholm_forecast", withExtension: "json") else {
            throw MockError.mockLoadError
        }
        
        let jsonData = try Data(contentsOf: fileUrl)
        let weatherData = try JSONDecoder().decode(WeeklyForecast.self, from: jsonData)
        
        return weatherData
    }
    
    func loadWeather(for location: CLLocationCoordinate2D) async throws -> WeeklyForecast {
        return try loadMockedData()
    }
    
    func loadWeather(for city: String) async throws -> WeeklyForecast {
        return try loadMockedData()
    }
}
