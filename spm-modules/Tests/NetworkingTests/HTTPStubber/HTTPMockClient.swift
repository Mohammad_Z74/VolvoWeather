//
//  HTTPMockClient.swift
//  
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import Foundation
import Networking

final class HTTPMockClient {
    
    static let shared = HTTPMockClient()
    
    private init() {}
    
    lazy var mockClient: HTTPClient = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockURLProtocol.self]
        let client = HTTPClient(configuration: configuration, baseURL: URL(string: "BASEURL")!)
        return client
    }()
}
