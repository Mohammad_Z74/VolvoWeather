//
//  HTTPRequestTests.swift
//  
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import XCTest
import NetworkingInterface
@testable import Networking

final class HTTPRequestTests: XCTestCase {
    
    private struct DummyRouter: NetworkingRouterProtocol {
        var method: HTTPMethod {
            .get
        }
        
        var path: String
        
        var headerFields: [String : String] = [:]
        
        var requestBody: HTTPBody? = nil
        
        init(path: String, requestBody: HTTPBody? = nil) {
            self.path = path
            self.requestBody = requestBody
        }
    }
    
    func testRequestBuilder() {
        // given
        let client = HTTPClient(baseURL: URL(string: "https://api.openweathermap.org/data/2.5/")!)
        var dummyBody: HTTPBody = HTTPBody(content: Data())
        dummyBody.params = [
            URLQueryItem(name: "q", value: "Stockholm"),
            URLQueryItem(name: "cnt", value: "15"),
            URLQueryItem(name: "units", value: "metric"),
            URLQueryItem(name: "appid", value: "1234")
        ]
        let dummyRouter: DummyRouter = DummyRouter(path: "forecast", requestBody: dummyBody)
        let request = HTTPRequest(router: dummyRouter)
        
        // when
        let urlRequest = try! request.urlRequest(inClient: client)
        
        // then
        XCTAssertEqual(urlRequest.url!.absoluteString, "https://api.openweathermap.org/data/2.5/forecast?q=Stockholm&cnt=15&units=metric&appid=1234")
    }
}
