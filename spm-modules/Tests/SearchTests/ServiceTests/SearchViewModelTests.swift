//
//  SearchViewModelTests.swift
//  
//
//  Created by Mo Zakizadeh on 2/26/23.
//

import Combine
import CoreLocation
import XCTest
import SearchInterface
@testable import Search

final class SearchViewModelTests: XCTestCase {
    
    private class SearchDummyDelegate: SearchDelegate {
        func updateHomeWith(loc: CLLocationCoordinate2D) {
            //
        }
    }
    
    private var sut: SearchViewModel!
    private var dummySearchDelegate: SearchDummyDelegate!
    
    override func setUp() {
        dummySearchDelegate = SearchDummyDelegate()
        sut = SearchViewModel(searchDelegate: dummySearchDelegate, dataLoader: SearchServiceDataMock())
        sut.citiesTableViewDataSource = CitiesTableViewDataSource(tableView: UITableView())
    }
    
    override func tearDown() {
        sut = nil
        dummySearchDelegate = nil
    }
    
    func testSearch() {
        // given
        let expectation = XCTestExpectation(description: "search")
        var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
        // when
        sut.searchForCity(city: "stock")
        sut.citiesTableViewDataSource?
            .$weathers
            .sink(receiveValue: { weather in
                if weather.isEmpty == false {
                    expectation.fulfill()
                }
            }).store(in: &disposables)
        
        // then
        wait(for: [expectation], timeout: 0.5)
    }
    
    func testSearchPublisher() {
        // given
        let expectation = XCTestExpectation(description: "searchPublisher")
        var disposables: Set<AnyCancellable> = Set<AnyCancellable>()
        // when
        sut.searchForCityPublisherCall(city: "stock")
        sut.citiesTableViewDataSource?
            .$weathers
            .sink(receiveValue: { weather in
                if weather.isEmpty == false {
                    expectation.fulfill()
                }
            }).store(in: &disposables)
        
        // then
        wait(for: [expectation], timeout: 0.5)
    }
}

