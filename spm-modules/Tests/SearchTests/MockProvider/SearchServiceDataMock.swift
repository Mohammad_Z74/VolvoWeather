//
//  SearchServiceDataMock.swift
//  
//
//  Created by Mo Zakizadeh on 2/26/23.
//

import Combine
import CoreLocation
import NetworkingInterface
@testable import Search

final class SearchServiceDataMock: SearchServiceProtocol {
    
    enum MockError: Error {
        case mockLoadError
    }
    
    private func loadMockedData() throws -> WeatherData {
        guard let fileUrl = Bundle.module.url(forResource: "search_stock", withExtension: "json") else {
            throw MockError.mockLoadError
        }
        
        let jsonData = try Data(contentsOf: fileUrl)
        let weatherData = try JSONDecoder().decode(WeatherData.self, from: jsonData)
        
        return weatherData
    }
    
    func loadDefaultCities() throws -> [WeatherData] {
        return []
    }
    
    func search(for city: String) async throws -> WeatherData {
        return try loadMockedData()
    }
    
    func search(for city: String) -> AnyPublisher<WeatherData, HTTPResponseError> {
        let weatherData: WeatherData? = try? loadMockedData()
        return Just(weatherData)
            .compactMap {$0}
            .setFailureType(to: HTTPResponseError.self)
            .eraseToAnyPublisher()
    }
}
