//
//  AppDelegate.swift
//  VolvoWeather
//
//  Created by Mo Zakizadeh on 2/20/23.
//

import UIKit

// MARK: App delegate
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Coordinator pattern to navigate between flows and inject dependencies.
        setupCoordinator()
        return true
    }
    
    // MARK: AppCoordinator
    fileprivate func setupCoordinator() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let navController = UINavigationController()
        
        let dependencyEngine: DependencyEngine = DependencyEngine(networkService: VLVNetworkingProvider())
        appCoordinator = AppCoordinator(navigationController: navController, window: window, dependencyEngine: dependencyEngine)
        appCoordinator.start()
    }
}
