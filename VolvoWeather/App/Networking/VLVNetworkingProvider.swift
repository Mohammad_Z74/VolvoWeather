//
//  VLVNetworkingProvider.swift
//  VolvoWeather
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Combine
import Foundation
import NetworkingInterface
import Networking

final class VLVNetworkingProvider: NetworkingInterface {
    
    private let networkLogger: HTTPLoggerProtocol
    
    static let baseURL: URL = URL(string: "https://api.openweathermap.org/data/2.5/")!
    
    init(logger: HTTPLoggerProtocol = VLVNetworkLogger.shared) {
        self.networkLogger = logger
    }
    
    func fetchData<T>(with router: NetworkingRouterProtocol, for dataType: T.Type) async throws -> T where T : Decodable {
        let client = HTTPClient(baseURL: Self.baseURL, networkLogger: networkLogger)
        let request = HTTPRequest(router: router)
        do {
            let response = try await request.fetch(client).decode(dataType.self)
            return response
        } catch let error as HTTPResponseError {
            throw error
        }
    }
    
    func fetchDataPublisher<T>(with router: NetworkingRouterProtocol, for dataType: T.Type) -> AnyPublisher<T, HTTPResponseError> where T : Decodable {
        let client = HTTPClient(baseURL: Self.baseURL, networkLogger: networkLogger)
        let request = HTTPRequest(router: router)
        let responsePublisher: AnyPublisher<T, HTTPResponseError> =
        request
            .fetch(client)
            .decodeDataPublisher(request: request)
            .eraseToAnyPublisher()
        
        return responsePublisher
    }
}
