//
//  AppCoordinator.swift
//  VolvoWeather
//
//  Created by Mo Zakizadeh on 2/20/23.
//

import UIKit
import Home
import Search
import SearchInterface
import Utilities

public final class AppCoordinator: NSObject, Coordinator, SearchRouterDelegate {
    
    let window: UIWindow?
        
    // Since AppCoordinator is top of all coordinators of our app, it has no parent coordinator
    public weak var parentCoordinator: Coordinator? = nil
    
    // ChildCoordinators of AppCoordinator
    public var childCoordinators: [Coordinator] = []
    
    public var navigationController: UINavigationController
    
    let dependencyEngine: DependencyEngine
    
    init(navigationController: UINavigationController, window: UIWindow?, dependencyEngine: DependencyEngine) {
        self.navigationController = navigationController
        self.window = window
        self.dependencyEngine = dependencyEngine
        super.init()
    }
    
    // App start
    public func start(animated: Bool) {
        guard let window = window else { return }
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        let homeCoordinator = HomeCoordinator(navigationController: navigationController, networkInterface: dependencyEngine.networkService, searchRouterDelegate: self)
        self.childCoordinators.append(homeCoordinator)
        homeCoordinator.parentCoordinator = self
        homeCoordinator.start()
    }
    
    public func navigateToSearchWith(delegate: SearchDelegate, animated: Bool = true) {
        let searchCoordinator: SearchCoordinator = SearchCoordinator(navigationController: navigationController, networkInterface: dependencyEngine.networkService)
        self.childCoordinators.append(searchCoordinator)
        searchCoordinator.parentCoordinator = self
        searchCoordinator.toSearch(searchDelegate: delegate, animated: animated)
    }
}
