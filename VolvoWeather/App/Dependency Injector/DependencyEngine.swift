//
//  DependencyEngine.swift
//  VolvoWeather
//
//  Created by Mo Zakizadeh on 2/21/23.
//

import Networking
import NetworkingInterface

final class DependencyEngine {
    
    var networkService: NetworkingInterface
    
    init(networkService: NetworkingInterface) {
        self.networkService = networkService
    }
    
}
