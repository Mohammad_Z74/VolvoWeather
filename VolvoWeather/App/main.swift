//
//  main.swift
//  VolvoWeather
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import UIKit

/// This function avoids calls for AppDelegate in UnitTest.
private func delegateClassName() -> String? {
    return NSClassFromString("XCTestCase") == nil ? NSStringFromClass(AppDelegate.self) : nil
}

let argc = CommandLine.argc
let argv = CommandLine.unsafeArgv
  _ = UIApplicationMain(argc, argv, nil, delegateClassName())


