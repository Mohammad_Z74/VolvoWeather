//
//  AppCoordinatorTests.swift
//  VolvoWeatherTests
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import CoreLocation
@testable import Home
@testable import Search
import SearchInterface
import UIKit
import XCTest
@testable import VolvoWeather

final class AppCoordinatorTests: XCTestCase {
    
    private final class DummySearchDelegate: SearchDelegate {
        func updateHomeWith(loc: CLLocationCoordinate2D) {
            // nothing
        }
    }
    
    var sut: AppCoordinator!
    
    override func setUp() {
        let navController: UINavigationController = UINavigationController()
        let window: UIWindow = UIWindow()
        let dependencyEngine: DependencyEngine = DependencyEngine(networkService: VLVMockNetworkProvider())
        sut = AppCoordinator(navigationController: navController, window: window, dependencyEngine: dependencyEngine)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testStart() {
        // given
        // when
        sut.start(animated: false)
        // then
        XCTAssertNotNil(sut.navigationController.visibleViewController as? HomeViewController)
        XCTAssertNotNil(sut.childCoordinators[0] as? HomeCoordinator)
    }
    
    func testSeachFlow() {
        // given
        let dummySearchDelegate: DummySearchDelegate = DummySearchDelegate()
        // when
        sut.navigateToSearchWith(delegate: dummySearchDelegate, animated: false)
        // then
        XCTAssertNotNil(sut.navigationController.visibleViewController as? SearchViewController)
        XCTAssertNotNil(sut.childCoordinators[0] as? SearchCoordinator)
    }
    
}
