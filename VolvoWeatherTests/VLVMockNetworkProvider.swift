//
//  VLVMockNetworkProvider.swift
//  VolvoWeatherTests
//
//  Created by Mo Zakizadeh on 2/24/23.
//

import Combine
import Foundation
import NetworkingInterface

final class VLVMockNetworkProvider: NetworkingInterface {
    
    func fetchData<T>(with router: NetworkingRouterProtocol, for dataType: T.Type) async throws -> T where T : Decodable {
        let data: T = try loadMockedData()
        return data
    }
    
    func fetchDataPublisher<T>(with router: NetworkingRouterProtocol, for dataType: T.Type) -> AnyPublisher<T, HTTPResponseError> where T : Decodable {
        let data: T? = try? loadMockedData()
        return Just(data)
            .compactMap {$0}
            .setFailureType(to: HTTPResponseError.self)
            .eraseToAnyPublisher()
    }
    
    // Always returns stokcholm_forecast.json in our case
    private func loadMockedData<T: Decodable>() throws -> T {
        guard let fileUrl = Bundle(for: VLVMockNetworkProvider.self).url(forResource: "stokcholm_forecast", withExtension: "json") else {
            throw MockError.mockLoadError
        }
        
        let jsonData = try Data(contentsOf: fileUrl)
        let mockedData = try JSONDecoder().decode(T.self, from: jsonData)
        
        return mockedData
    }
    
    enum MockError: Error {
        case mockLoadError
    }
}
