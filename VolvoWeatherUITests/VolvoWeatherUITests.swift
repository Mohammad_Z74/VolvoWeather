//
//  VolvoWeatherUITests.swift
//  VolvoWeatherUITests
//
//  Created by Mo Zakizadeh on 2/20/23.
//

import XCTest

final class VolvoWeatherUITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    func testSearchFlow() throws {
        // Given
        let app = XCUIApplication()
        // When
        app.buttons["Search"].tap()
        // Then
        XCTAssert(app.staticTexts["Choose a city"].exists)
        XCTAssert(app.staticTexts["Select or search a city to see the weather"].exists)
        XCTAssert(app.textFields["SearchCityTextField"].exists)
    }
    
    func testDefaultCitiesExistence() throws {
        // given
        let app = XCUIApplication()
        // when
        app.buttons["Search"].tap()
        // then
        XCTAssert(app.tables.cells.staticTexts["Stockholm"].exists)
        XCTAssert(app.tables.cells.staticTexts["Gothenburg"].exists)
        XCTAssert(app.tables.cells.staticTexts["Mountain View"].exists)
        XCTAssert(app.tables.cells.staticTexts["London"].exists)
        XCTAssert(app.tables.cells.staticTexts["New York"].exists)
        XCTAssert(app.tables.cells.staticTexts["Berlin"].exists)
    }
    
    func testSearchCitySelectUpdateInHome() throws {
        // given
        let app = XCUIApplication()
        // when
        app.buttons["Search"].tap()
        app.tables.cells.staticTexts["Gothenburg"].tap()
        // then
        XCTAssert(app.staticTexts["Gothenburg, SE"].exists)
        XCTAssert(app.staticTexts["Now,"].exists)
    }
    
    func testSearchForCity() throws {
        // given
        let app = XCUIApplication()
        // when
        app.buttons["Search"].tap()
        app.textFields.element.tap()
        app.textFields.element.typeText("Mexico")
        app.tables.cells.staticTexts["Mexico"].tap()
        // then
        XCTAssert(app.staticTexts["Mexico, PH"].exists)
        XCTAssert(app.staticTexts["Now,"].exists)
    }
    
}
